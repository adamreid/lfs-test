# Git LFS Test #
This is a small repo with a single static image used to demo what happens if the LFS object`
on the gitlab server goes missing.


_*WARNING*_: This procedure could result in users of gitlab being unable to access LFS files if
the files sha256 sum begins with 531e. *DO NOT DO THIS ON PRODUCTION GITLAB INSTANCES*

## Scenario 1 ##
In this scenario the stored objects are removed from the gitlab lfs storage backend and attempts to push
them back up are unsuccessful.

1. Clone this repo.
2. Push this repo to your gitlab server.
3. Move directory 53/1e directory from it's original location on configured gitlab lfs storage
   backend to somewhere else. For example `mv /lfs-objects/53/1e /tmp`
4. git push to the gitlab server. The objects missing from the gitlab server are not pushed up again.
5. Check the lfs storage backend and the objects will still be missing.
6. Attempt to push all LFS objects again with tracing turned on. Note that missing objects are not skipped for
   upload

```
GIT_TRACE=2 GIT_CURL_VERBOSE=2 git lfs push --all nb master
23:49:17.020638 git.c:576               trace: exec: git-lfs push --all nb master
23:49:17.021258 run-command.c:646       trace: run_command: git-lfs push --all nb master
23:49:17.077929 trace git-lfs: run_command: 'git' version
23:49:17.193200 trace git-lfs: run_command: 'git' config -l
23:49:17.320900 trace git-lfs: tq: running as batched queue, batch size of 100
23:49:17.321969 trace git-lfs: run_command: ssh -- git@git.navblue.tech git-lfs-authenticate areid/lfs-test.git upload
23:49:23.640118 trace git-lfs: HTTP: POST https://git.navblue.tech/areid/lfs-test.git/info/lfs/locks/verify
23:49:24.428368 trace git-lfs: HTTP: 200
23:49:24.486155 trace git-lfs: HTTP: {"ours":[],"theirs":[]}
Locking support detected on remote "nb". Consider enabling it with:
  $ git config lfs.https://git.navblue.tech/areid/lfs-test.git/info/lfs.locksverify true
23:49:24.488189 trace git-lfs: Upload refs [master] to remote nb
23:49:31.420219 trace git-lfs: run_command: git rev-list --objects --do-walk master --
23:49:31.521918 trace git-lfs: run_command: git cat-file --batch
23:49:31.580615 trace git-lfs: tq: sending batch of size 1
23:49:31.581360 trace git-lfs: ssh cache: git@git.navblue.tech git-lfs-authenticate areid/lfs-test.git upload
23:49:31.582165 trace git-lfs: api: batch 1 files
23:49:31.582828 trace git-lfs: HTTP: POST https://git.navblue.tech/areid/lfs-test.git/info/lfs/objects/batch
23:49:31.669045 trace git-lfs: HTTP: 200
23:49:31.727479 trace git-lfs: HTTP: {"objects":[{"oid":"531e5b427811b939c882921bb3d8e789d083b05c854b6c7357dccf1126187869","size":409546}]}
23:49:31.729197 trace git-lfs: tq: starting transfer adapter "basic"
Git LFS: (0 of 0 files, 1 skipped) 0 B / 0 B, 399.95 KB skipped
```
## Scenario 2 ##
In this scenario to try and get their lfs objects back into gitlab users make a copy of their local repo
and create a new repo in gitlab to push the copy to. The idea is that they could perhaps trick gitlab into
accepting the objects. The result is unsuccesful because gitlab believes it already has the object.

Steps 1-3 in scenario 1 are required before the following steps.
1. copy local repo from scenario 1 to another directory
2. Create a new project in gitlab
3. cd into new local copy and add new project as the remote
4. Attempt to push all LFS objects and the local copies of the files will be skipped because gitlab believes
   they exist locally.

```
GIT_TRACE=2 GIT_CURL_VERBOSE=2 git lfs push --all origin master
23:42:21.534623 git.c:576               trace: exec: git-lfs push --all origin master
23:42:21.535304 run-command.c:646       trace: run_command: git-lfs push --all origin master
23:42:21.596462 trace git-lfs: run_command: 'git' version
23:42:21.711448 trace git-lfs: run_command: 'git' config -l
23:42:21.832609 trace git-lfs: tq: running as batched queue, batch size of 100
23:42:21.833878 trace git-lfs: run_command: ssh -- git@git.navblue.tech git-lfs-authenticate areid/lfs-test-copy.git upload
23:42:24.875652 trace git-lfs: HTTP: POST https://git.navblue.tech/areid/lfs-test-copy.git/info/lfs/locks/verify
23:42:25.292513 trace git-lfs: HTTP: 200
23:42:25.354268 trace git-lfs: HTTP: {"ours":[],"theirs":[]}
Locking support detected on remote "origin". Consider enabling it with:
  $ git config lfs.https://git.navblue.tech/areid/lfs-test-copy.git/info/lfs.locksverify true
23:42:25.356720 trace git-lfs: Upload refs [master] to remote origin
23:42:32.347443 trace git-lfs: run_command: git rev-list --objects --do-walk master --
23:42:32.448134 trace git-lfs: run_command: git cat-file --batch
23:42:32.538622 trace git-lfs: tq: sending batch of size 1
23:42:32.539543 trace git-lfs: ssh cache: git@git.navblue.tech git-lfs-authenticate areid/lfs-test-copy.git upload
23:42:32.540450 trace git-lfs: api: batch 1 files
23:42:32.541162 trace git-lfs: HTTP: POST https://git.navblue.tech/areid/lfs-test-copy.git/info/lfs/objects/batch
23:42:32.633887 trace git-lfs: HTTP: 200
23:42:32.695215 trace git-lfs: HTTP: {"objects":[{"oid":"531e5b427811b939c882921bb3d8e789d083b05c854b6c7357dccf1126187869","size":409546}]}
23:42:32.697501 trace git-lfs: tq: starting transfer adapter "basic"
Git LFS: (0 of 0 files, 1 skipped) 0 B / 0 B, 399.95 KB skipped
```
